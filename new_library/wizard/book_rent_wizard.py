from odoo import models, api, fields


class LibraryRentWizard(models.TransientModel):
    _name = 'library.rent.wizard'

    borrower_id = fields.Many2one('res.partner', string='Borrower')
    book_id = fields.Char(string='Book_id')

    def add_book_rents(self):
        pass
