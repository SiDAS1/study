from odoo import models, api, fields
from odoo.exceptions import ValidationError, UserError


class NewLibraryBook(models.Model):
    _name = 'new.library.book'
    _description = 'New Library Book'
    _inherit = ['hrp.base.wkf.interface']

    name = fields.Char('Title', required=True)
    date_release = fields.Date('Release Date', required=True)
    author_ids = fields.Many2many(comodel_name='library.book.author',
                                  relation='new_library_book_library_book_author_rel',
                                  column1='new_library_book_id',
                                  column2='library_book_author_id')
    state = fields.Selection([('draft', 'draft'),
                              ('submitted', 'submitted'),
                              ('approving', 'approving'),
                              ('approved', 'approved'),
                              ('adjusting', 'adjusting'),
                              ('adjusted', 'adjusted'),
                              ('failed', 'failed'),
                              ('refused', 'refused'),
                              ('canceled', 'canceled')],
                             string='State', default='draft')
    cover = fields.Binary('Book Cover')
    borrower_id = fields.Many2one(comodel_name='res.partner', sring='Borrower')

    @api.multi
    def action_submit(self):
        for rec in self:
            # if not rec.line_ids:
            #     raise ValidationError('ERROR')
            rec.state = "submitted"
            self.env['hrp.base.wkf'].create_or_restart_wkf(self._name, rec.id)
