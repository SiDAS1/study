from odoo import models, fields, api


class AuthorCompany(models.Model):
    _name = "book.author.company"

    name = fields.Char(string='Company name')
    author_ids = fields.One2many(comodel_name='library.book.author',
                                 inverse_name='company_ids',
                                 string='Authors')
