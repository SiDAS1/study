from odoo import models, fields, api


class BookAuthor(models.Model):
    _name = 'library.book.author'

    author_name = fields.Char('Author Name')
    company_ids = fields.Many2one(comodel_name='book.author.company',
                                  string='Company')
