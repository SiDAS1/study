{
    'name': 'New library',
    'summary': 'The new library',
    'description': """
    The new library
    """,
    'author': 'DAS',
    'website': "http://www.hand-china.com",
    'category': 'Uncategorized',
    'version': '0.1',

    'depends': ['dy_base', 'base', 'decimal_precision'],

    'data': [
        'views/library_book.xml',
        'views/library_book_author.xml',
        'views/author_company.xml',
        'wizard/library_book_rent_wizard.xml',
        'security/ir.model.access.csv',
    ],
    'application': True,
}
